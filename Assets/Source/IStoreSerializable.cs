﻿using Newtonsoft.Json;

namespace Source
{
    public interface IStoreSerializable
    {
        [JsonIgnore]
        string Name { get; }
    }
}