﻿using System;

namespace Source
{
    [Serializable]
    public class GeneratedItems : IStoreSerializable
    {
        public string[] Items { get; set; }
        public string Name => "Store";
    }
}