﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Source
{
    [Serializable]
    public class Session : IStoreSerializable
    {
        public List<string> DisplayedItems { get; set; } = new List<string>();
        public long? CreationTimeStamp { get; set; }
        [JsonIgnore] public string Name => "Session";
    }
}