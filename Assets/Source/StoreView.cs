﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Source
{
    public class StoreView : MonoBehaviour
    {
        [SerializeField] private Button _rerollBtn;
        [SerializeField] private Button[] _buttons;
        private TextMeshProUGUI[] _text = new TextMeshProUGUI[10];

        public event Action<int> OnBtnClick;
        public event Action OnRerollClick;

        public void SetButtonState(int index, bool interactable, string name)
        {
            _buttons[index].interactable = interactable;
            _text[index].text = name;
        }

        public void Initialize()
        {
            _rerollBtn.onClick.AddListener(() => OnRerollClick?.Invoke());
            for (var index = 0; index < _buttons.Length; index++)
            {
                var index1 = index;
                _buttons[index].onClick?.AddListener(() => OnBtnClick?.Invoke(index1));
                _text[index] = _buttons[index].GetComponentInChildren<TextMeshProUGUI>();
            }
        }
    }
}