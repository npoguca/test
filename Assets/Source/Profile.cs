﻿using System;
using System.Collections.Generic;

namespace Source
{
    [Serializable]
    public class Profile : IStoreSerializable
    {
        public List<string> PurchasedItems { get; set; } = new List<string>();
        public string Name => "Profile";
    }
}