﻿using UnityEngine;

namespace Source
{
    public class EntryPoint : MonoBehaviour
    {
        [SerializeField] private StoreView _storeView;

        static EntryPoint()
        {
            var loc = new Locator();
        }

        private void Awake()
        {
            Locator.Instance.BindView(_storeView);
            var controller = Locator.Instance.Get<StoreController>();
            controller.Initialize();
        }
    }
}