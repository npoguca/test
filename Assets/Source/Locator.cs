﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Source
{
    public class Locator
    {
        public static Locator Instance { get; private set; }
        private readonly Dictionary<Type, object> _services;

        public Locator()
        {
            Instance = this;
            _services = new Dictionary<Type, object>
            {
                {typeof(JsonSerializer), new Serializer()},
                {typeof(StoreController), new StoreController()},
                {typeof(Serializer), new Serializer()},
            };
        }

        public T Get<T>()
        {
            if (_services.TryGetValue(typeof(T), out var impl))
            {
                return (T) impl;
            }

            throw new Exception($"{typeof(T)} is missing!");
        }

        public void BindView<T>(T view)
        {
            _services.Add(view.GetType(), view);
        }
    }
}