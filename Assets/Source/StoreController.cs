﻿using System;
using System.Collections.Generic;
using System.Linq;
using Random = UnityEngine.Random;

namespace Source
{
    public class StoreController : IDisposable
    {
        private List<string> _displayedItems;

        public void Initialize()
        {
            var sessions = Locator.Instance.Get<Serializer>().Deserialize<Session>() ?? new Session();
            if (sessions.CreationTimeStamp != null &&
                new TimeSpan(DateTime.UtcNow.Ticks - sessions.CreationTimeStamp.Value).Hours >= 24)
            {
                GenerateSession();
            }

            _displayedItems = sessions.DisplayedItems;
            var storeView = Locator.Instance.Get<StoreView>();
            Locator.Instance.Get<StoreView>().Initialize();
            UpdateItems();
            storeView.OnBtnClick += OnPurchase;
            storeView.OnRerollClick += () =>
            {
                GenerateSession();
                UpdateItems();
            };
        }

        private void OnPurchase(int index)
        {
            var serializer = Locator.Instance.Get<Serializer>();
            var profile = serializer.Deserialize<Profile>();
            var session = serializer.Deserialize<Session>();
            profile.PurchasedItems.Add(session.DisplayedItems[index]);
            serializer.Serialize(profile);
            UpdateItems();
        }

        private void UpdateItems()
        {
            var serializer = Locator.Instance.Get<Serializer>();
            var session = serializer.Deserialize<Session>();
            var profile = serializer.Deserialize<Profile>();
            for (var index = 0; index < 10; index++)
            {
                var elementAtOrDefault = session.DisplayedItems.ElementAtOrDefault(index);
                Locator.Instance.Get<StoreView>()
                    .SetButtonState(index,
                        elementAtOrDefault != null && !profile.PurchasedItems.Contains(elementAtOrDefault),
                        elementAtOrDefault ?? string.Empty);
            }
        }


        private void GenerateSession()
        {
            var serializer = Locator.Instance.Get<Serializer>();
            var selected = serializer.Deserialize<GeneratedItems>().Items
                .Except(serializer.Deserialize<Profile>().PurchasedItems)
                .ToList();
            var newList = new List<string>();
            while (selected.Count > 0 && newList.Count < 10)
            {
                var index = Random.Range(0, selected.Count);
                var items = selected[index];
                newList.Add(items);
                selected.RemoveAt(index);
            }

            var session = new Session
            {
                CreationTimeStamp = DateTime.UtcNow.Ticks,
                DisplayedItems = newList
            };
            serializer.Serialize(session);
        }

        public void Dispose()
        {
            var storeView = Locator.Instance?.Get<StoreView>();
            if (storeView == null)
            {
                return;
            }

            storeView.OnBtnClick -= OnPurchase;
            storeView.OnRerollClick -= OnReRoll;
        }

        private void OnReRoll()
        {
            GenerateSession();
            UpdateItems();
        }
    }
}