﻿using System;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;

namespace Source
{
    public class Serializer
    {
        public const string Session = "Session";
        public const string Store = "Store";

        private const string AssetsPath = "Assets/Configs/";

        public T Deserialize<T>() where T : IStoreSerializable
        {
            var instance = (T) Activator.CreateInstance(typeof(T));
            var name = typeof(T).GetProperty("Name")?.GetValue(instance).ToString() ??
                       throw new Exception($"Failed to deserialize {typeof(T)}");
            var path = GetPath(name);
            if (!File.Exists(path))
            {
                return instance;
            }

            var readAllText = File.ReadAllText(path);

            instance = JsonConvert.DeserializeObject<T>(readAllText);
            return instance;
        }

        private static string GetPath(string name)
        {
            return Path.Combine(AssetsPath, $"{name}.json");
        }

        public void Serialize<T>(T obj) where T : IStoreSerializable
        {
            File.WriteAllText(GetPath(obj.Name), JsonConvert.SerializeObject(obj));
        }
    }

    public static class MenuItems
    {
        [MenuItem("Items/Generate")]
        public static void Generate()
        {
            if (Application.isPlaying)
            {
                throw new Exception("Unable to generate items at runtime!");
            }

            var genItems = new GeneratedItems
            {
                Items = Enumerable.Range(0, 100).Select(x => $"Item_{x}").ToArray()
            };

            var ser = new Serializer();
            ser.Serialize(genItems);
        }

        [MenuItem("Items/Reset")]
        public static void Reset()
        {
            if (Application.isPlaying)
            {
                throw new Exception("Unable to generate items at runtime!");
            }

            var ser = new Serializer();
            ser.Serialize(new GeneratedItems());
            ser.Serialize(new Session());
            ser.Serialize(new Profile());
            Generate();
        }
    }
}